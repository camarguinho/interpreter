package auto.estudo;

public class Programa {

	public static void main(String[] args) {
		Expressao esquerda = new Soma(new Numero(5), new Numero(10));
		Expressao direita = new Subtracao(new Numero(10), new Numero(5));
		Expressao soma = new Soma(esquerda, direita);
		
		System.out.println(soma.avalia());
	}
	
}
