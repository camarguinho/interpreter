package auto.estudo;

public class Soma implements Expressao{
	
	private Expressao expressaoEsquerda;
	private Expressao expressaoDireita;

	public Soma(Expressao expressaoEsquerda, Expressao expressaoDireita){
		this.expressaoEsquerda = expressaoEsquerda;
		this.expressaoDireita = expressaoDireita;
	}

	@Override
	public int avalia() {
		int valorEsquerda = this.expressaoEsquerda.avalia();
		int valorDireita = this.expressaoDireita.avalia();
		return valorEsquerda + valorDireita;
	}
	
	
}
